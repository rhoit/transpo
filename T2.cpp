#include<iostream>
using namespace std;

# include <ncurses.h>

# define inity 0
# define initx 0
# define cell_width 10
# define cell_height 4
# define dtab cell_width-2

class Transportation { //NorthWest
private:
  int eg_flag;
  int uv_flag; //indicate uv is obtained
  
  int S;
  int *D;
  
  int *u; char *umask;
  int *v; char *vmask;
  
  int mx_i, mx_j; //max i, max j;
  int mi_i, mi_j; //min i, min j;
  int X, Y;
  
public:
  unsigned short m, n;
  int ** tb;
  int ** akate; //allocation
  char ** akmask;
  char ** pathakm;
  int TotalCost;
  
  int * scr; //Source
  int * dmd; //Demand
  
  void grid(int, int, int, int, int, int);
  Transportation();
  ~Transportation();
  
  void solve();
  void show();
  void maxalloc();
  void chk_bv(int, int);
  int chk_nv(); //reports to move in next iteration
  void loop();
  int nav(int, int, char);

  int ** eg_NW();
};

int main() {
  initscr();
  getch();
  Transportation t;
  t.eg_NW();
  t.solve();
  refresh();
  getch();
  endwin();
  return 0;
}

void Transportation::grid(int starty, int startx, int lines, int cols, int tile_width, int tile_height) {
  int endy, endx, i, j;
  
  endy = starty + lines * tile_height;
  endx = startx + cols  * tile_width;
  
  for(j = starty; j <= endy; j += tile_height)
    for(i = startx; i <= endx; ++i)
      mvaddch( j, i, ACS_HLINE);
  for(i = startx; i <= endx; i += tile_width)
    for(j = starty; j <= endy; ++j)
      mvaddch( j, i, ACS_VLINE);
  mvaddch( starty, startx, ACS_ULCORNER);
  mvaddch( endy, startx, ACS_LLCORNER);
  mvaddch( starty, endx, ACS_URCORNER);
  mvaddch( 	endy, endx, ACS_LRCORNER);
  for(j = starty + tile_height; j <= endy - tile_height; j += tile_height) {	mvaddch( j, startx, ACS_LTEE);
    mvaddch( j, endx, ACS_RTEE);
    for(i = startx + tile_width; i <= endx - tile_width; i += tile_width)
      mvaddch( j, i, ACS_PLUS);
  }
  for(i = startx + tile_width; i <= endx - tile_width; i += tile_width) {	mvaddch(starty, i, ACS_TTEE);
    mvaddch(endy, i, ACS_BTEE);
  }
}

Transportation::Transportation() {
  m=0; n=0;
  eg_flag=0;
  
  scr=NULL;
  dmd=NULL;
  
  tb=NULL;
  akate=NULL;
  u=NULL;
  v=NULL;
}

Transportation::~Transportation() {
  //	if(akate!=NULL)
  //	{
  //		for(int i=0; i<n; i++)
  //			delete []akate[i];
  //		delete [] akate;
  //	}
  
  if(eg_flag==1) return; //Avoid the Segmentation fault deallocating the static var
  
  if(scr!=NULL) delete [] scr;
  if(dmd!=NULL) delete [] dmd;
  
  if(tb!=NULL) {
    for(int i=0; i<n; i++)
      delete []tb[i];
    delete [] tb;
  }
}

void Transportation::show() {
  if(tb==NULL) {
    printw("Can't Print Table: NUll pointer\n");
    return;
  }
  
  grid(inity,initx,m,n,cell_width,cell_height);
  
  attron(A_BOLD);
  for(int i=0; i<m; i++) {
    for(int j=0; j<n; j++)
      mvprintw(i*cell_height+1,j*cell_width+1,"%*d", dtab, tb[i][j]);
    mvprintw(i*cell_height+cell_height/2,n*cell_width+1,"%d", scr[i]);
  }
  for(int i=0; i<n; i++) mvprintw(m*cell_height+1,i*cell_width+cell_width/2,"%d", dmd[i]);
  attroff(A_BOLD);
  printw("\n");
}

void Transportation::solve() {
  //Allocate the values
  D=new int[n];
  
  akate=new int * [m];
  akmask=new char * [m];
  pathakm=new char * [m];
  for(int i=0; i<m; i++) {
    akate[i]=new int [n];
    akmask[i]=new char [n];
    pathakm[i]=new char [n];
  }

  show();
  getch();
  
  attron(A_UNDERLINE);
  printw("North West Method:");
  attroff(A_UNDERLINE);
  printw("\nMax Allocation\n");
  maxalloc();
  
  u=new int [m]; umask=new char [m]; umask[0]='d';	u[0]=0;
  v=new int [n]; vmask=new char [n];
  
 next_iter:
  uv_flag=0;
  for(int i=1; i<m; i++) umask[i]=NULL;
  for(int i=0; i<n; i++) vmask[i]=NULL;
  for(int i=0; i<m; i++)
    for(int j=0; j<n; j++)
      pathakm[i][j]=NULL;
  
  getyx(stdscr, Y, X);
  for(int i=0; i<m; i++)
    for(int j=0; j<n; j++) {
      if(akmask[i][j]=='b') chk_bv(i,j);
      mvprintw(i*cell_height+cell_height-1,j*cell_width+2,"%-2d", akate[i][j]);
      mvprintw(i*cell_height+1,j*cell_width+2,"%c", akmask[i][j]);
    }
  move(Y,X);
  getch();
  //cheak all u v are obtained
  
  printw("uv_flag :%d\n", uv_flag);
  
  while(uv_flag!=0) {
    uv_flag--;
    for(int i=0; i<m; i++)
      for(int j=0; j<n; j++)
	if(akmask[i][j]=='b') chk_bv(i,j);
  }

  //uv Display
  for(int i=0; i<m; i++) printw("u%d:%d \t",i,u[i]);
  for(int i=0; i<n; i++) printw("v%d:%d \t",i,v[i]);
  
  if(chk_nv()>0) {
    akmask[mx_i][mx_j]='b';
    printw("\nEntering var: x[%d][%d]\n", mx_i, mx_j);
    loop();
  }
  else {
    printw("\nIts over");
    TotalCost=0;
    for(int i=0; i<m; i++)
      for(int j=0; j<n; j++)
	if(akmask[i][j]=='b')
	  TotalCost+=akate[i][j]*tb[i][j];
    printw("\nTotal Cost: %d", TotalCost);
    return;
  }

  printw("\nLeaving var: x[%d][%d]\n", mi_i, mi_j);
  printw("Do u wanna change it: ");
  switch(getch()) {
  case 'Y':
  case 'y':
    printw("\ni: ");
    scanw("%d",&mi_i);
    printw("j: ");
    scanw("%d",&mi_j);
  }
  akmask[mi_i][mi_j]=NULL;
  
  clear();
  show();
  goto next_iter;
}

int min;
int theta;
int a, b;

void Transportation::loop() {
  if(scr[mx_i]<dmd[mx_j]) theta=scr[mx_i];
  else theta=dmd[mx_j];
  
  akate[mx_i][mx_j]-=theta;
  a=-1; b=-1;
  
  nav(mx_i, mx_j, 'S');
}

int Transportation::nav(int i, int j, char dir) {
  char tdri, tdr, rollbak;
  if(dir!='S' && i==mx_i && j==mx_j) {
    printw("loop completed\nBacktrace: ");
    ::min=theta; //entering variable can't never the leaving var
    akate[i][j]+=theta;
    return 'C';
  }
  
 top:
  if(dir!='D' && rollbak !='U') {
    for(int x=i-1; x>=0; x--) {
      if(akmask[x][j]=='b' && pathakm[x][j]!='x') {
	i=x;
	tdr='U';
	tdri='D';
	goto end;
      }
    }
  }
  
  if(dir!='L'&& rollbak !='R') {
    for(int y=j+1; y<n; y++) {
      if(akmask[i][y]=='b'&& pathakm[i][y]!='x') {
	j=y;
	tdr='R';
	tdri='L';
	goto end;
      }
    }
  }
  
  if(dir!='U'&& rollbak !='D') {
    for(int x=i+1; x<m; x++) {
      if(akmask[x][j]=='b'&& pathakm[x][j]!='x') {
	i=x;
	tdr='D';
	tdri='U';
	goto end;
      }
    }
  }
  
  if(dir!='R'&& rollbak !='L') {
    for(int y=j-1; y>=0; y--) {
      if(akmask[i][y]=='b'&& pathakm[i][y]!='x') {
	j=y;
	tdr='L';
	tdri='R';
	goto end;
      }
    }
  }
  return 2; //rollback
  
 end:
  printw("-%c->[%d,%d]", tdr, i, j);
  
  getyx(stdscr, Y, X);
  mvprintw(0,69,"a:%-2d b:%-2d", a, b);
  mvprintw(1,69,"i:%-2d j:%-2d", i, j);
  mvprintw(2,69,"dir:%c tdr:%c", dir, tdr);

  pathakm[i][j]='x';
  mvprintw(i*cell_height+cell_height/2,j*cell_width+cell_width/2,"%c", pathakm[i][j]);
  
  if(rollbak!=NULL) rollbak=0;
  
  getch();
  move(Y,X);
  switch(nav(i,j,tdr)) {
  case 'C':
    if(dir=='S') printw("[%d,%d]-%c->[%d,%d]",i,j,tdri,mx_i,mx_j);
    else printw("[%d,%d]-%c->",i,j,tdri);
    
    akate[i][j]+=theta;
    theta*=-1;
    
    if(akate[i][j]<0) akate[i][j]=0;
    getyx(stdscr, Y, X);
    
    for(int p=1; p<cell_height; p++)
      mvchgat(i*cell_height+p,j*cell_width+1, dtab+1, A_STANDOUT, 1, NULL);
    mvchgat(i*cell_height+1,j*cell_width+1, dtab, A_STANDOUT|A_BOLD , 1, NULL);
    
    if(a!=-1 && b!=-1) {
      int p,l;
      if(b!=j) {
	l=b-j;
	if(l<0) {
	  l=j*cell_width;
	  p=(b+1)*cell_width;
	}
	else {
	  l=b*cell_width;
	  p=(j+1)*cell_width;
	}
	
	for(;p<=l;p++) mvaddch( i*cell_height+cell_height/2, p, ACS_HLINE);
      }
      
      if(a!=i) {
	l=a-i;
	if(l<0) {
	  l=i*cell_height;
	  p=(a+1)*cell_height;
	}
	else {
	  l=a*cell_height;
	  p=(i+1)*cell_height;
	}
	
	for(;p<=l;p++)
	  mvaddch(p,j*cell_width+cell_width/2, ACS_VLINE);
      }
    }
    
    attron(A_STANDOUT);
    mvprintw(i*cell_height+cell_height-1,j*cell_width+2,"%-2d", akate[i][j]);
    attroff(A_STANDOUT);
    getch();
    move(Y,X);
    a=i; b=j;
    
    if(::min>akate[i][j]) {
      ::min=akate[i][j];
      mi_i=i;
      mi_j=j;
    }
    if(tdr==dir) return 'A'; //Again same
    return 'C';
    
  case 'A':	return 'C'; //again same
  case 2:
    return 1;
  case 1:
    printw("[RollB<-%c-(%d,%d)]", tdr,i,j);
    getyx(stdscr, Y, X);
    mvprintw(i*cell_height+cell_height/2,j*cell_width+cell_width/2,"%c", pathakm[i][j]);
    getch();
    move(Y,X);
    
    rollbak=tdr;
    
    goto top;
  }
}

int Transportation::chk_nv() {
  int max=0, tmp;
  for(int i=0; i<m; i++) {
    for(int j=0; j<n; j++) {
      if(akate[i][j]!=0) continue;
      tmp=u[i]+v[j]-tb[i][j];
      if(tmp>max) {
	max=tmp;
	mx_i=i;
	mx_j=j;
      }
    }
  }
  //	printw("\nmax C[%d][%d]: %d\n", mx_i, mx_j, max);
  return max;
}

void Transportation::chk_bv(int i, int j) {
  if(umask[i]!='d' && vmask[j]!='d') {
    uv_flag++;
    return;
  }

  if(umask[i]!='d') {
    u[i]=tb[i][j]-v[j];
    umask[i]='d';
  }
  else if(vmask[j]!='d') {
    v[j]=tb[i][j]-u[i];
    vmask[j]='d';
  }
}

void Transportation::maxalloc() {
  for(int i=0; i<n; i++) D[i]=dmd[i];
  
  for(int i=0; i<m; i++) {
    S=scr[i];
    for(int j=0; j<n; j++) {
      if(S>D[j]) akate[i][j]=D[j];
      else akate[i][j]=S;
      
      S-=akate[i][j];
      D[j]-=akate[i][j];
      
      if(akate[i][j]!=0) akmask[i][j]='b';
    }
  }
}

int ** Transportation::eg_NW() {
  eg_flag=1;
  m=3; n=4;
  int nw[3][4] = {
    10,	0, 20, 11,
    12, 7, 9,  20,
    0, 14,16, 18,
  };

  int s[3]={ 15, 25, 5 };
  int d[4]={ 5, 15, 15, 10 };
  scr=new int [m];
  dmd=new int [n];
  
  tb=new int * [m];
  for(int i=0; i<m; i++) {
    tb[i]=new int [n];
    scr[i]=s[i];
  }

  for(int j=0; j<n; j++) {
    for(int i=0; i<m; i++)
      tb[i][j]=nw[i][j];
    dmd[j]=d[j];
  }
  
  return tb;
}
